'use strict';

let config = {
	optimize: false,
    sourceMaps: false,

	files: {
		javascripts: {
			joinTo: {
				'js/lib.js': /^(?!app)/,
				'js/app.js': [
					/^app\/js\/*/,
					/^app\/modules\/js\/*/
				]
			}
		},
		stylesheets: {
			joinTo: {
				'css/app.css': /^app\/css\/app\.css/,
				'css/app.desktop.css': /^app\/css\/app\.desktop\.css/,
				'css/lib.css': /^(?!app)/
			}
		},
		templates: {
			joinTo: {
				'js/app.js': /^app\/modules\/components\/*/
			}
		}
	},

	paths: {
		public: './assets'
	},
/*
	npm: {
		enabled: true,
		globals: {
			$: 'jquery',
			jQuery: 'jquery'
		},
		aliases: {
			//vue: "vue/dist/vue.runtime.esm.js"
			jquery: 'jquery/dist/jquery.slim.js'
		}
	},
*/
	plugins: {
		babel: {
			pattern: /\.(js|vue)$/,
			presets: ['@babel/preset-env'],
			ignore: [
				/^node_modules/
			]
		},
		copyfilemon:{
			"fonts": ["app/fonts"],
			"images": ["app/images"]
		},
		postcss: {
			processors: [
				require('postcss-import'),
				require('postcss-extend'),
				require('postcss-color-function'),
				require('precss'),
				require('autoprefixer')
			]
		},
		vue: {
			extractCSS: false,
			out: 'app/css/app/6-components.compiled.css'
		}
	}
}


// Compress CSS
if ( process.env.NODE_ENV === 'production' ) {
	config.plugins.postcss.processors.push(require('cssnano')({ safe:true }));

	// Compile Vue CSS
	config.plugins.vue.extractCSS = true;
}


module.exports = config;
