## 1. Initialization

### Nodejs & Package Managers
If you don’t have it already, install [NodeJs](https://nodejs.org/en/) and make sure npm is up to date. [PNPM](https://github.com/pnpm/pnpm) is an alternative package manager which uses symlinks so it only saves one copy on disk.
```
npm install -g npm
```

### Configure the project
If the site has a `package.json` already then we simply want to install it's dependencies
```
npm install
```
Otherwise we need to create a new one
```
npm init -y
```

## 2. JS
```
npm install brunch babel-brunch @babel/preset-env @babel/plugin-transform-runtime @babel/runtime uglify-js-brunch --save-dev
```

## 3. Vuejs
```
npm install github:dgozick/vue-brunch --save-dev
npm install vue --save

```

## 4. CSS
```
npm install postcss-brunch postcss postcss-import postcss-extend postcss-color-function precss@2.0.0 autoprefixer cssnano --save-dev
```

## 5. Static Assets
```
npm install copyfilemon-brunch --save-dev
```


## ALL
```
npm install brunch babel-brunch @babel/preset-env @babel/plugin-transform-runtime @babel/runtime uglify-js-brunch github:dgozick/vue-brunch postcss-brunch postcss postcss-import postcss-extend postcss-color-function precss@2.0.0 autoprefixer cssnano copyfilemon-brunch --save-dev
npm install vue --save
```
