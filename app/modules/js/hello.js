import Vue from 'vue'
import hello from 'modules/components/hello.vue'

let helloApp = new Vue({
	el: '#helloApp',
	render(h) {
		return h(hello, {
			props: {

			}
		})
	},
	methods: {
		init() {
		}
	},
	data: {}
});

module.exports = helloApp;
