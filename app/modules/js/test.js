import Vue from 'vue'
import test from 'modules/components/test.vue'

let testApp = new Vue({
	el: '#testApp',
	render(h) {
		return h(test, {
			props: {

			}
		})
	},
	methods: {
		init() {
		}
	},
	data: {}
});

module.exports = testApp;
