// Libraries
import Vue from 'vue';

let app = {
	element: document.querySelectorAll('html')[0],
	width: 0,
	height: 0,
	isPortrait : 0,
	isMobile: false,
	profile: "",
	components: {},

	init() {
		// Setup Functions
		this.update();
		this.setup();

		return this;
	},

	setup() {
		// Event Buses
		this.eventBus = new Vue();

		// Events
		this.event.add(window, "resize", this.update.bind(this));
		this.event.add(window, "scroll", this.headerScroll.bind(this));

		// Lazy Loader
		this.lazyLoader();

		// Sticky header
		this.headerScroll();
	},

	update(event) {
		this.setAppDimensions();
		this.setProfile();
	},

	setAppDimensions() {
		this.width = window.innerWidth || document.body.offsetWidth || document.documentElement.offsetWidth;
		this.height = window.innerHeight || document.body.offsetHeight || document.documentElement.offsetHeight;
		this.isPortrait = window.screen.orientation ? Math.abs(window.screen.orientation.angle) != 90 : this.width > this.height;
	},

	setProfile() {
		let breakpoint = 768,
			profile = "";

		if ( (this.isPortrait && this.width > breakpoint) || (!this.isPortrait && this.height > breakpoint) ) {
			profile = "desktop";
		} else {
			profile = "mobile";
		}

		if ( this.profile != profile ) {
			if ( profile == "desktop") {
				this.element.classList.add("desktop");
				this.element.classList.remove("mobile");

				this.isMobile = false;
			} else {
				this.element.classList.add("mobile");
				this.element.classList.remove("desktop");

				this.isMobile = true;
			}

			this.profile = profile;

			document.dispatchEvent(new CustomEvent("app-profileChange"));
		}
	},

	headerScroll() {
		if ( window.pageYOffset > 0 && !this.element.classList.contains('scrolling') ) {
			this.element.classList.add('scrolling');
		} else if ( window.pageYOffset <= 0 ) {
			this.element.classList.remove('scrolling');
		}
	},

	lazyLoader: function() {
		// TODO: add onload function on img tags to set an attribute so that
		// 		 it doesn't replace the src on images that already have been loaded.

		var elements = document.querySelectorAll('main img, main iframe');

		if (('IntersectionObserver' in window)) {
			this.observer = new IntersectionObserver(entries => {

				for (i in entries) {
					var e = entries[i];

					if (e.isIntersecting) {
						var o = e.target;
						var lazySrc = o.getAttribute('data-src');

						if (lazySrc != null) {
							o.setAttribute('src', lazySrc);
							o.removeAttribute('data-src');
						}

						this.observer.unobserve(o);
					}
				}
			}, {});


			for(var i = 0; i < elements.length; i++) {
				if ( !elements[i].hasAttribute('data-src') ){
					elements[i].setAttribute('data-src', elements[i].getAttribute('src'));
					elements[i].removeAttribute('src');
				}

				this.observer.observe(elements[i]);
			}
		} else {
			for(var i = 0; i < elements.length; i++) {
				if ( elements[i].hasAttribute('data-src') && !elements[i].hasAttribute('src')){
					elements[i].setAttribute('src', elements[i].getAttribute('data-src'));
					elements[i].removeAttribute('data-src');
				}
			}
		}
	},

	event: {
		add(object, type, callback) {
			if (object == null || typeof(object) == 'undefined') {
				return;
			}

			object.addEventListener(type, callback, false);
		},

		remove(object, type, callback) {
			if (object == null || typeof(object) == 'undefined') {
				return;
			}

			object.removeEventListener(type, callback, false);
		},

		trigger(object, name, data) {
			if (object == null || typeof(object) == 'undefined') {
				return;
			}


			// Dispatch/Trigger/Fire the event
			let event = new CustomEvent(name, data)
			//object.dispatchEvent(event);
		}
	},

	data: {
		cache: new Map(),

		has(key) {
			return this.cache.has(key);
		},

		set(key, value) {
			this.cache.set(key, value);
		},

		get(key) {
			return this.cache.get(key);
		}
	}
};

module.exports = app;
